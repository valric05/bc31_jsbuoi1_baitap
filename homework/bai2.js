/*
 *Input: Nhập vào 5 số thực
 *Process:
 *    Tính tổng 5 số thực
 *    Lấy tổng chia cho 5
 *Output: Trung bình cộng của 5 số thực
 */

var a = 2.6;
var b = 133.5;
var c = 17.9;
var d = 43.7;
var e = 78.2;

var sum = a + b + c + d + e;

var avr = sum / 5;

console.log(avr);
