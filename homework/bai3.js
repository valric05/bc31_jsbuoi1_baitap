/*
 *Input:
 *    Số tiền tính bằng USD
 *    Tỉ giá từ USD sang VND là 23.500
 *Process:
 *    Lấy số tiền USD nhân cho tỉ giá
 *Output:
 *   Số tiền tính theo đơn vị VND */

var currencyUSD = 30;
var USDtoVND = 23500;

var currencyVND = currencyUSD * USDtoVND;

console.log(currencyVND + " VND");
