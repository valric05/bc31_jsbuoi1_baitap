/**
 * Input: Số có 2 chữ số
 * Process:
 *      Lấy ra chữ số thứ hai
 *      Lấy ra chữ số thứ nhất
 *      Cộng 2 chữ số lại
 * Output: Tổng 2 ký số từ số đã nhập
 */

var number = 43;

var secondChar = number % 10;
var firstChar = (number - secondChar) / 10;

var output = firstChar + secondChar;

console.log(output);
