/*
 *Input: Chiều dài và chiều rộng HCN
 *Process:
 *    Tính diện tích, lấy chiều dài nhân chiều rộng
 *    Tính chhu vi, lấy chiều dài cộng chiều rộng rồi nhân hai
 *Output:
 *    Diện tích, chu vi HCN
 */

var width = 3;
var length = 7;

var area = width * length;
var parameter = (width + length) * 2;

console.log("Diện tích: " + area);
console.log("Chu vi: " + parameter);
